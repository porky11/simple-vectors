use crate::Vector;
use num_traits::{real::Real, Zero};
use token_parser::{Parsable, Parser, Result as ParseResult, Unit};

impl<C, T: Real + Parsable<C>, const N: usize> Parsable<C> for Vector<T, N> {
    fn parse_list<I: Iterator>(parser: &mut Parser<I>, context: &C) -> ParseResult<Vector<T, N>>
    where
        I::Item: Into<Unit<I>>,
    {
        let mut result: Vector<T, N> = Vector::zero();
        for i in 0..N {
            result[i] = parser.parse_next(context)?;
        }
        Ok(result)
    }
}
